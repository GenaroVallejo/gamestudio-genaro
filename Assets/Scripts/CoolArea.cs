﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoolArea : MonoBehaviour {
			
		
	public Text Life;
	private LifeCat healthScript;
	public RandomAudio coolAudio;
	public RandomAudio destroyAudio;



	void Start () {

	
	}


		void OnTriggerEnter2D (Collider2D col){

		healthScript = col.GetComponent<LifeCat> ();
			if (col.gameObject.name == "Catice") {
		     healthScript.melting = false;
			coolAudio.PlayRandomized();
			CheckMelt();
	
			}
		}
		
		void OnTriggerExit2D (Collider2D col){
		healthScript = col.GetComponent<LifeCat> ();
			if (col.gameObject.name == "Catice") {
			healthScript.melting = true;
			}
		}
		
		
		
		void CheckMelt ()
	{

		Debug.Log ("MeltWait");
			Invoke ("MeltedArea", 5);
		}


	void MeltedArea ()

	{
		destroyAudio.PlayRandomized ();
		healthScript.melting = true;
		Destroy (this.gameObject);
	}

}


