﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LifeCat : MonoBehaviour {

	public int LifeCount;
	public Text Life;
	public int MaxLife;
	public int MinLife;
	public bool melting;

	public float tickRate = .5f;
	private float currentTickTime;

	void Start ()
	{
		LifeCount = 60;
		MaxLife = 60;
		MinLife = 0;
		melting = true;
		
	}


	void Update () {

	if (LifeCount == 0) {
		Destroy (this.gameObject);
	}
	
	if (LifeCount <= MinLife)
	{
		LifeCount = MinLife;
		Life.text = LifeCount.ToString ();
		
	}
	
	if (LifeCount >= MaxLife) {
			LifeCount = MaxLife;
			Life.text = LifeCount.ToString ();

		}

		if (Time.time > currentTickTime) {
			currentTickTime = Time.time + tickRate;
			if (melting == true)
			{
				LifeCount--;
				Life.text = LifeCount.ToString ();
			} else 
			{
				LifeCount +=7;
				Life.text = LifeCount.ToString ();
				
			}
		}
		
	}

	public void FlameDamage () {

		LifeCount -=5;
		Life.text = LifeCount.ToString ();
	}
}
