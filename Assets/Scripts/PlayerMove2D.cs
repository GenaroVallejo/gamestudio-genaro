﻿using UnityEngine;
using System.Collections;

public class PlayerMove2D : MonoBehaviour {

	public float speed = 2;
	
	public bool facingLeft = false;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetKey (KeyCode.RightArrow)) {
			transform.position += Vector3.right * speed * 2 * Time.deltaTime;
			if (facingLeft == true) {
				Vector3 theScale = transform.localScale;
				theScale.x *= -1;
				transform.localScale = theScale;
				facingLeft = false;
			}
		}  
		if (Input.GetKey (KeyCode.LeftArrow)) {
			transform.position += Vector3.left * speed * 2 * Time.deltaTime;
			if (facingLeft == false) {
				Vector3 theScale = transform.localScale;
				theScale.x *= -1;
				transform.localScale = theScale;
				facingLeft = true;
			}
		}  
		if (Input.GetKey (KeyCode.UpArrow)) {
			transform.position +=Vector3.up * speed * 2 * Time.deltaTime;
			
		}
		
		if (Input.GetKey (KeyCode.DownArrow)) {
			transform.position +=Vector3.down * speed * 2 * Time.deltaTime;
		}
	}
}
