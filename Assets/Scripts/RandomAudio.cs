﻿using UnityEngine;
using System.Collections;

public class RandomAudio: MonoBehaviour {
	
	public AudioClip[] coolSounds; 
	
	private AudioSource source;
	private float vollowRange = .5f;
	private float volHighRange = 1.0f;
	private float pitchLow = .9f;
	private float pitchHigh = 1.1f;
	
	void Awake ()
	{
		source = GetComponent <AudioSource> ();
	}
	
	public void PlayRandomized ()
	{
		float volScaler = Random.Range (vollowRange, volHighRange);
		float pitch = Random.Range (pitchLow, pitchHigh);
		source.pitch = pitch;
		source.PlayOneShot (coolSounds[Random.Range (0, coolSounds.Length)], source.volume * volScaler);
	}
}