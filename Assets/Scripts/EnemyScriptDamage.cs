﻿using UnityEngine;
using System.Collections;

public class EnemyScriptDamage : MonoBehaviour {

	public int startingHealth = 3;
	public int damage = 1;
	public RandomAudio enemyAudio;
	public RandomAudio explosionAudio;
	
	public int currentHealth;
	
	void Start()
	{
		currentHealth = startingHealth;
		
	}
	
	public void Damage()
	{

		currentHealth -= damage;
		enemyAudio.PlayRandomized ();
		if (currentHealth <= 0)
			
		{
			explosionAudio.PlayRandomized ();
			Defeated();
		}
	}
	
	void Defeated()
	{
		gameObject.SetActive (false);
	}
	
}