﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CollisioAction : MonoBehaviour {

	public Text coinsText;
	public Text levelText;
	private int LevelUp;
	public int coinsCollected;
	private LifeCat maxHealthUp;
	public RandomAudio coinAudio;
	public RandomAudio levelUpAudio;
	public GameObject levelMessage;


	void Start () {
	
		coinsCollected = 0;
		LevelUp = 1;
		levelMessage.SetActive (false);

	}




	void OnCollisionEnter2D(Collision2D other)
	{
		
		ShootingMechanics cp = other.gameObject.GetComponent<ShootingMechanics> ();

		
		if (cp != null) {
			
			switch (cp.myProp) {
			case ShootingMechanics.PropType.blue:
				DoBlue ();
				break;
			case ShootingMechanics.PropType.red:
				DoRed ();
				break;
			case ShootingMechanics.PropType.enemy:
				DoDamage ();
				break;
			case ShootingMechanics.PropType.collectible:
				Collect (other.gameObject, cp);
				break;
			}


			if (cp.myProp == ShootingMechanics.PropType.enemy) {
				DoDamage ();
			} else if (cp.myProp == ShootingMechanics.PropType.collectible) {
				Collect (other.gameObject, cp);
				coinAudio.PlayRandomized ();
			}
		}
	}
	
	public void DoRed()
	{
		Debug.Log ("hit red");
	}
	
	public void DoBlue()
	{
		Debug.Log ("hit blue");
	}
	
	public void DoDamage()
	{
		LifeCat LifePoints = this.gameObject.GetComponent<LifeCat>();
		LifePoints.FlameDamage ();
	}
	
	public void Collect(GameObject objectToCollect, ShootingMechanics objectCp)
	{

		if (objectCp.collected == false) 
		{
			objectCp.collected = true;
			Debug.Log ("collected: " + Time.time);
			coinsCollected += 1;
			coinsText.text = coinsCollected.ToString ();
			objectToCollect.SetActive (false);


			if (coinsCollected == 50) 
			{
				maxHealthUp = this.gameObject.GetComponent<LifeCat> ();
				maxHealthUp.MaxLife += 5;
				LevelUp++;
				levelUpAudio.PlayRandomized ();
				levelText.text = LevelUp.ToString ();
				levelMessage.SetActive (true);
				Invoke ("turnOffMessage", 3);

			}

			if (coinsCollected == 100) 
			{
				maxHealthUp = this.gameObject.GetComponent<LifeCat> ();
				maxHealthUp.MaxLife += 5;
				LevelUp++;
				levelUpAudio.PlayRandomized ();
				levelText.text = LevelUp.ToString ();
				levelMessage.SetActive (true);
				Invoke ("turnOffMessage", 3);
			}
		}
	}

	void turnOffMessage ()
	{
		levelMessage.SetActive (false);
	}
}