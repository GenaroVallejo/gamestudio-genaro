﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextScript : MonoBehaviour {

	public Text textUI1;
	public Text textUI2;
	public Text textUI3;
	public Text textUI4;
	public GameObject enemy;

	// Use this for initialization
	void Start () {

		textUI1.enabled = true;
		textUI2.enabled = true;
		textUI3.enabled = false;
		textUI4.enabled = false;
	
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D col) {

		if (col.gameObject.tag == "CoolArea1") 
		{
			Invoke ("disableUI", 3);
			Invoke ("enableShoot", 2);
		}
	}

	void disableUI ()
	{
		textUI1.enabled = false;
		textUI2.enabled = false;
	}

	void enableShoot ()
	{
		textUI3.enabled = true;
		Invoke ("disableShoot", 5);
	}

	void disableShoot ()
	{
		textUI3.enabled = false;
		Invoke ("enableCoins", 2);
	}

	void enableCoins ()
	{
		textUI4.enabled = true;
		Invoke ("disableCoins", 5);
	}

	void disableCoins ()
	{
		textUI4.enabled = false;
	}
}


