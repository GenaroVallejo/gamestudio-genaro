﻿using UnityEngine;
using System.Collections;

public class DestroyBullets : MonoBehaviour {



	void Start () {

		Invoke ("byeBullet", 1);

	}

	void byeBullet ()
	{
		Destroy (this.gameObject);
	}


	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D other) {

		if (other.tag == "Wall") 
		{
			Destroy(this.gameObject);
		}

		if (other.tag == "Enemy") {
			EnemyScriptDamage DamageEnemy = other.gameObject.GetComponent<EnemyScriptDamage> ();
			DamageEnemy.Damage();
			Destroy(this.gameObject);
		}
	}
}
