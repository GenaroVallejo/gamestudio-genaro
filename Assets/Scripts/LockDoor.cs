﻿using UnityEngine;
using System.Collections;

public class LockDoor : MonoBehaviour {

	public GameObject BlockTile;

	// Use this for initialization
	void Start () {
	
		BlockTile.SetActive (false);
	}
	
	// Update is called once per frame
	void OnTriggerExit2D (Collider2D col) {
	
		if (col.gameObject.name == "Catice") {
			BlockTile.SetActive (true);
		}
	}
}
