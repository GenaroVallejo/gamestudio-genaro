﻿using UnityEngine;
using System.Collections;

public class ShootIce : MonoBehaviour {
	
	public Rigidbody2D projectile;
	public float bulletSpeed = 250;
	public Transform bulletSpawn;
	public float fireRate = .25f;
	private float nextFire;
	public RandomAudio icyAudio;

	private PlayerMove2D playerMove;
	
	// Use this for initialization
	void Awake () 
	{
		playerMove = GetComponent<PlayerMove2D> ();	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetButton ("Fire1") && Time.time > nextFire) 
		{
			Shoot();
			icyAudio.PlayRandomized ();
		}
		
	}
	
	void Shoot()
	{
		nextFire = Time.time + fireRate;
		Rigidbody2D clone = Instantiate (projectile, bulletSpawn.position, transform.rotation) as Rigidbody2D;


		if (playerMove.facingLeft) 
		{
			clone.AddForce (Vector2.left * bulletSpeed);
		} 
		else 
		{
			clone.AddForce (Vector2.right * bulletSpeed);
		}
	}
}


